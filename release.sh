#!/bin/bash

checkout_release () {

    git fetch
    git pull

    LATEST_BRANCH_VERSION=0
    RELEASE_BRANCHES=$(git branch -a | grep release | cut -d "/" -f 4)

    for BRANCH in $(echo ${RELEASE_BRANCHES}); do

        BRANCH_VERSION=$(echo $BRANCH | cut -d "." -f 1);
        if [ $LATEST_BRANCH_VERSION -lt $BRANCH_VERSION ]; then
            LATEST_BRANCH_VERSION=$(echo $BRANCH_VERSION);
        fi;
    done;

    RELEASE_BRANCH_NAME="release/$LATEST_BRANCH_VERSION.0.0"
    git checkout $RELEASE_BRANCH_NAME
}

update_release_version () {
    VERSION_FILE='./version.txt'
    VERSION=$(cat $VERSION_FILE)
    
    MAJOR_VERSION=$(echo $VERSION | cut -d . -f 1)
    MINOR_VERSION=$(echo $VERSION | cut -d . -f 2)
    PATCH_VERSION=$(echo $VERSION | cut -d . -f 3)

    NEW_RELEASE_MINOR=$(($MINOR_VERSION + 1))
    NEW_RELEASE_VERSION="${MAJOR_VERSION}.${NEW_RELEASE_MINOR}.${PATCH_VERSION}"
    echo $NEW_RELEASE_VERSION > $VERSION_FILE
    echo "Release new version: $(cat $VERSION_FILE)"
}

push_to_release () {
    git add $VERSION_FILE
    git commit -m "Updated version to $(cat $VERSION_FILE)"
    git push -u origin $RELEASE_BRANCH_NAME
}

echo -e "\nChecking out branch..."
checkout_release

echo -e "\nUpdating version..."
update_release_version

echo -e "\nPushing to branch..."
push_to_release