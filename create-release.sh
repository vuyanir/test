#!/bin/bash

checkout_dev () {
    git fetch
    git pull
    git checkout develop
}

create_release_branch () {
    VERSION_FILE='./version.txt'
    VERSION=$(cat $VERSION_FILE)
    MAJOR_VERSION=$(echo $VERSION | cut -d . -f 1)

    git checkout -b "release/${MAJOR_VERSION}.0.0" develop
    git push -u origin "release/${MAJOR_VERSION}.0.0"
}

update_dev_version () {
    NEW_DEV_MAJOR=$(($MAJOR_VERSION + 1))
    NEW_DEV_VERSION="${NEW_DEV_MAJOR}.0.0"
    
    echo $NEW_DEV_VERSION > $VERSION_FILE
    echo "Dev new version: $(cat $VERSION_FILE)"
}

push_to_dev () {
    git add $VERSION_FILE
    git commit -m "Updated version to $(cat $VERSION_FILE)"
    git push -u origin develop
}


echo -e "\nChecking out develop..."
checkout_dev

echo -e "\nCreating release branch..."
create_release_branch

echo -e "\nChecking out develop..."
checkout_dev

echo -e "\nUpdating dev version number..."
update_dev_version

echo -e "\nPushing to develop..."
push_to_dev